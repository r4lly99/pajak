package com.ganesha.djp;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;

import com.ganesha.channel.BinaryX25Channel;

public class SendMessage {

	// static final String hostname = "10.245.194.200";
	// static int portNumber = 8266;

	static final String hostname = "192.168.0.88";
	static int portNumber = 1212;

	public static void main(String[] args) throws ISOException, IOException {

		ISOPackager packager = new GenericPackager("cfg/packager/iso87djp.xml");
		// Logger logger = new Logger();
		// logger.addListener (new Listener (System.out));
		BinaryX25Channel channel = new BinaryX25Channel(hostname, portNumber,
				packager);
		// ((LogSource)channel).setLogger (logger, “test-channel”);
		channel.connect();
		System.out.println(channel.isConnected());

		// ISOMsg networkReq = new ISOMsg();
		// networkReq.setHeader("ISO011000017".getBytes());
		// networkReq.setMTI("0800");
		// networkReq
		// .set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		// networkReq.set(11, "000052");
		// networkReq.set(70, "001");
		// channel.send(networkReq);
		// ISOMsg r = channel.receive();

		ISOMsg inquiry = new ISOMsg();
		inquiry.setHeader("ISO011000017".getBytes());
		inquiry.setMTI("0200");
		inquiry.set(2, "0000000000000000");
		inquiry.set(3, "300000");
		inquiry.set(4, "000221301000");
		inquiry.set(7, new SimpleDateFormat("MMddHHmmss").format(new Date()));
		inquiry.set(12, "140130");
		inquiry.set(13, "0111");
		inquiry.set(15, "0111");
		inquiry.set(18, "7012");
		inquiry.set(32, "0161");
		inquiry.set(37, "000000000701");
		inquiry.set(41, "0000000000000000");
		inquiry.set(47, "000000000000000000");
		inquiry.set(48, "0154795200730000411126100");
		inquiry.set(49, "360");
		inquiry.set(63, "000001");
		channel.send(inquiry);
		ISOMsg r = channel.receive();

		// ISOMsg inquiry = new ISOMsg();
		// inquiry.setHeader("ISO011000017".getBytes());
		// inquiry.setMTI("0200");
		// inquiry.set(2, "0000000000000000");
		// inquiry.set(3, "300000");
		// inquiry.set(4, "000221301000");
		// inquiry.set(7, new SimpleDateFormat("MMddHHmmss").format(new
		// Date()));
		// inquiry.set(12, "140130");
		// inquiry.set(13, "0111");
		// inquiry.set(15, "0111");
		// inquiry.set(18, "7012");
		// inquiry.set(32, "0161");
		// inquiry.set(37, "000000000701");
		// inquiry.set(41, "0000000000000000");
		// inquiry.set(47, "000000000000000000");
		// inquiry.set(48, "0154795200730000411126100");
		// inquiry.set(49, "360");
		// inquiry.set(63, "000001");
		// channel.send(inquiry);
		// ISOMsg r = channel.receive();

		System.out.println("Reply :" + new String(r.pack()));
		channel.disconnect();

	}
}
