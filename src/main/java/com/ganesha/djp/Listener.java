package com.ganesha.djp;

import java.io.IOException;

import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.Q2;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;

import com.ganesha.utils.Constant;

public class Listener implements ISORequestListener, Configurable {

	private String queueName;
	protected long timeout;
	private Space<String, Context> sp;

	public Listener() {
		super();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setConfiguration(Configuration cfg)
			throws ConfigurationException {
		sp = SpaceFactory.getSpace(cfg.get("space"));
		queueName = cfg.get("queue");
		timeout = cfg.getLong("timeout");
	}

	@Override
	public boolean process(ISOSource source, ISOMsg m) {
		Context ctx = new Context();
		ctx.put(Constant.REQUEST_87, m);
		ctx.put(Constant.SOURCE_87, source);
		sp.out(queueName, ctx, timeout);
		return true;
	}

	public static void startQ2() throws IOException {
		Q2 q2 = new Q2();
		q2.start();
	}

	public static void main(String[] args) {
		try {
			startQ2();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ISOUtil.sleep(50000);
	}

}
