package com.ganesha.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;

import com.ganesha.model.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/jpos-rest/v1/")
public class TestController extends RestListener implements LogSource,
		Configurable {

	public TestController(ISOPackager packager) {
		super(packager);
	}

	@POST
	@Path(value = "account-status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountStatus(final String input) {
		try {
			String responseJson = "[{'msg':'this is response'}]";
			return Response.status(Response.Status.OK).entity(responseJson)
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@GET
	@Path(value = "account-get")
	@Produces(MediaType.APPLICATION_JSON)
	public Object accountGet() {
		try {
			Person p = new Person();
			p.setNAME("Rully");
			p.setLOCATION("Wisma Hayam Wuruk");
			Gson gson = new GsonBuilder().create();
			return gson.toJson(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Gagal";
	}

	@Override
	public void setConfiguration(Configuration cfg)
			throws ConfigurationException {
		// TODO Auto-generated method stub

	}

}