package com.ganesha.model;

public class Person {

	private String NAME;
	private String LOCATION;

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getLOCATION() {
		return LOCATION;
	}

	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}

	@Override
	public String toString() {
		return NAME + " - " + LOCATION;
	}

}
