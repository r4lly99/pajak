package com.ganesha.channel;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.util.Vector;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.ISOFilter.VetoException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.X25Channel;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

import com.ganesha.utils.JposUtil;

public class BinaryX25Channel extends X25Channel {

	BufferedReader reader = null;
	protected byte[] header;

	public BinaryX25Channel() {
		super();
	}

	public BinaryX25Channel(String hostname, int portNumber,
			ISOPackager packager) {
		super(hostname, portNumber, packager);
	}

	public BinaryX25Channel(ISOPackager p) throws IOException {
		super(p);
	}

	/**
	 * @return a byte array with the received message
	 * @exception IOException
	 */
	@Override
	protected byte[] streamReceive() throws IOException {
		int c, k = 0, len = 1;
		Vector<byte[]> v = new Vector<byte[]>();

		c = serverIn.read();
		if (c == -1)
			throw new EOFException("connection closed");
		byte[] b = new byte[1];
		b[0] = (byte) c;
		v.addElement(b);

		// Wait for packets until timeout
		while ((c = serverIn.available()) > 0) {
			b = new byte[c];
			if (serverIn.read(b) != c)
				throw new EOFException("connection closed");
			v.addElement(b);
			len += c;
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}

		byte[] d = new byte[len];
		for (int i = 0; i < v.size(); i++) {
			b = v.elementAt(i);
			System.arraycopy(b, 0, d, k, b.length);
			k += b.length;
		}
		return binToHexa(d);
	}

	/**
	 * This method is for replace 'active bit indicator' from binary to hex used
	 * when channel receive request
	 * 
	 * @param fullMsg
	 * @return
	 */
	private byte[] binToHexa(byte[] fullMsg) {
		String fullMsgString = new String(fullMsg);
		String mti = fullMsgString.substring(0, 4);
		String body = fullMsgString.substring(4);

		String bitData = "";
		String activeBitIndicator = "";

		try {
			if (body.startsWith("1")) {
				bitData = JposUtil.subString(body, 4 * 32);
				activeBitIndicator = JposUtil.subString(body, 0, 4 * 32);
			} else {
				bitData = JposUtil.subString(body, 4 * 16);
				activeBitIndicator = JposUtil.subString(body, 0, 4 * 16);
			}
		} catch (ISOException e) {
			e.printStackTrace();
		}

		String[] splitStringEvery = JposUtil.splitStringEvery(
				activeBitIndicator, 4);
		StringBuilder sb = new StringBuilder();
		for (String string : splitStringEvery) {
			sb.append(JposUtil.binaryToHexa(string));
		}
		String convertedBit = sb.toString();
		fullMsg = (mti + convertedBit + bitData).getBytes();
		return fullMsg;
	}

	/**
	 * this method is format 'active bit indicator' from hex to bit for DJP
	 * match ISO format used when send response
	 * 
	 * @param reqMsg
	 * @return
	 */
	private byte[] hexToBinary(byte[] reqMsg) {
		String reqMsgString = new String(reqMsg);
		String mti = reqMsgString.substring(0, 4);
		String body = reqMsgString.substring(4);
		char firstPrimaryHex = body.charAt(0);
		String firstPrimaryBit = JposUtil.hexaToBinary(String
				.valueOf(firstPrimaryHex));

		String primaryBitmapHex = "";
		String bitData = "";

		if (firstPrimaryBit.startsWith("1")) {
			primaryBitmapHex = body.substring(0, 32);
			bitData = body.substring(32);
		} else {
			primaryBitmapHex = body.substring(0, 16);
			bitData = body.substring(16);
		}

		String[] splittedPrimaryBitmap = JposUtil.splitStringEvery(
				primaryBitmapHex, 1);
		StringBuilder primaryBitmapBit = new StringBuilder();

		for (int i = 0; i < splittedPrimaryBitmap.length; i++) {
			primaryBitmapBit.append(JposUtil
					.hexaToBinary(splittedPrimaryBitmap[i]));
		}

		String formatedReqMsg = mti + primaryBitmapBit.toString() + bitData;
		return formatedReqMsg.getBytes();
	}

	@Override
	public void send(ISOMsg m) throws IOException, ISOException {
		LogEvent evt = new LogEvent(this, "send");
		try {
			if (!isConnected())
				throw new ISOException("unconnected ISOChannel");
			m.setDirection(ISOMsg.OUTGOING);
			m = applyOutgoingFilters(m, evt);
			evt.addMessage(m);
			m.setDirection(ISOMsg.OUTGOING); // filter may have drop this info
			m.setPackager(getDynamicPackager(m));
			byte[] b = m.pack();

			// here format active bit indicator from hex to Bin
			b = hexToBinary(b);
			setHeader(m.getHeader());
			evt.addMessage("Formated-Message", ISOUtil.dumpString(getHeader())
					+ ISOUtil.dumpString(b));
			synchronized (serverOutLock) {
				sendMessageLength(b.length + getHeaderLength(m));
				sendMessageHeader(m, b.length);
				sendMessage(b, 0, b.length);
				sendMessageTrailler(m, b);
				serverOut.flush();
			}
			cnt[TX]++;
			setChanged();
			notifyObservers(m);
		} catch (VetoException e) {
			// if a filter vets the message it was not added to the event
			evt.addMessage(m);
			evt.addMessage(e);
			throw e;
		} catch (ISOException e) {
			evt.addMessage(e);
			throw e;
		} catch (IOException e) {
			evt.addMessage(e);
			throw e;
		} catch (Exception e) {
			evt.addMessage(e);
			throw new ISOException("unexpected exception", e);
		} finally {
			Logger.log(evt);
		}
	}

}
